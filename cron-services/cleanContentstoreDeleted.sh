#!/bin/ash

CS_DELETED=/contentstore.deleted/
DELETED_DAYS_DIR=$(($DELETED_DAYS + 20))

if [[ -d "${CS_DELETED}" && ! -L "${CS_DELETED}" ]] ; then
    # Remove all files from contentstore.deleted older than DELETED_DAYS days
    find $CS_DELETED -type f -mtime +$DELETED_DAYS | xargs rm 2> /dev/null
    # Remove all empty folders from contentstore.deleted older than 50 days
    find $CS_DELETED -type d -mtime +$DELETED_DAYS_DIR -empty | xargs rm -r 2> /dev/null
fi

# Log cleaner to the alfresco.log file
if $LOGS ; then
    echo "$(date +'%Y-%m-%d %H:%M:%S,000') INFO [Alfresco-cron-services] BIN files of the deleted content older then $DELETED_DAYS days have been removed." >> /logs/alfresco/alfresco.log
fi
